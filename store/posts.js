export const state = () => ({
  postList: [],
  onePost: null,
  comment: []
});

export const mutations = {
  setPostList(state, payload) {
    state.postList = payload;
  },
  setPostComment(state, payload) {
    state.comment = payload;
  },
  setCurrentPost(state, payload) {
    state.onePost = payload;
  },
  updatePost(state, payload) {
    state.onePost.body = payload.body
  }
};

export const actions = {};

export const getters = {
  getPosts: s => s.postList,
  getCurrentPost: s => s.onePost,
  getPostComment: s => s.comment,
};
